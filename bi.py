import numpy as np
import scipy as sp

import matplotlib.pyplot as plt

x = np.linspace(0,6,100)

y = np.sin(x)

plt.plot(x, y)

plt.show()

