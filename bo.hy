(import os)
(import math)
(import tqdm)
(import pyro)
(import torch)
(import gpytorch)

(import [pyro.infer.mcmc [NUTS MCMC]])
(import [gpytorch.priors [LogNormalPrior NormalPrior UniformPrior]])

(import [pandas :as pd])
(import [numpy :as np])
(import [h5py :as h5])
(import [matplotlib [pyplot :as plt]])
(import [sklearn.preprocessing [MinMaxScaler minmax-scale]])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])

(setv hdf-name "../data/90nm_gmid.h5")

(setv raw (pd.DataFrame 
  (with [hdf-file (h5.File hdf-name "r")]
    (dfor c (.keys hdf-file)
      [c (->> c (get hdf-file) (np.array))]))))

(setv mat-name "../data/elevators.mat")

(import [scipy.io [loadmat]])
(setv data (-> mat-name (loadmat) (get "data") (torch.Tensor)))

;;;;;;;;;;; DKL ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;(setv X (-> data (get (, (slice None) (slice None -1))) 
;                 (minmax-scale) 
;                 (torch.from-numpy)
;                 (.float)))
;
;(setv Y (-> data (get (, (slice None) -1)) 
;                 (minmax-scale) 
;                 (torch.from-numpy)
;                 (.float)))

(setv raw-shuffled (.sample raw :frac 1))

(setv raw-x (. (np.vstack [raw-shuffled.gmid.values
                           (np.log10 raw-shuffled.fug.values)]) T))

(setv raw-y (np.log10 raw-shuffled.jd.values))

(.fit (setx scale-x (MinMaxScaler)) raw-x)
(.fit (setx scale-y (MinMaxScaler)) (.reshape raw-y -1 1))

(setv X (-> raw-x (scale-x.transform) (torch.from-numpy) (.float)))
(setv Y (-> raw-y (.reshape -1 1) (scale-y.transform) (.flatten) (torch.from-numpy) (.float)))

;(.fit (setx scale-x (MinMaxScaler)) (-> raw-shuffled (get ["gmid" "fug"]) (. values)))
;(.fit (setx scale-y (MinMaxScaler)) (-> raw-shuffled (get ["jd"]) (. values)))
;(setv X (-> raw-shuffled (get ["gmid" "fug"]) 
;                         (. values) 
;                         (scale-x.transform) 
;                         (torch.from-numpy) 
;                         (.float))
;      Y (-> raw-shuffled (get ["jd"]) 
;                         (. values) 
;                         (scale-y.transform) 
;                         (torch.from-numpy) 
;                         (torch.flatten)
;                         (.float)))

(setv train-n (int (math.floor (* 0.8 (len X))))
      train-x (-> X (get (, (slice None train-n) (slice None))) (.contiguous) (.cuda))
      valid-x (-> X (get (, (slice None train-n) (slice None))) (.contiguous))
      train-y (-> Y (get (slice None train-n)) (.contiguous) (.cuda)) 
      valid-y (-> Y (get (slice None train-n)) (.contiguous)))

(setv data-dim (.size train-x -1))

(defclass LFE [torch.nn.Sequential]
  (defn __init__ [self]
    (-> LFE (super self) (.__init__))
    (self.add-module "Linear1" (torch.nn.Linear data-dim 1000))
    (self.add-module "relu1" (torch.nn.ReLU))
    (self.add-module "Linear2" (torch.nn.Linear 1000 500))
    (self.add-module "relu2" (torch.nn.ReLU))
    (self.add-module "Linear3" (torch.nn.Linear 500 50))
    (self.add-module "relu3" (torch.nn.ReLU))
    (self.add-module "Linear4" (torch.nn.Linear 50 2))))

(setv fe (LFE))

(defclass GPRM [gpytorch.models.ExactGP]
  (defn __init__ [self train-x train-y likelihood]
    (-> GPRM (super self) (.__init__ train-x train-y likelihood))
    (setv self.mean-module (gpytorch.means.ConstantMean)
          self.covar-module (gpytorch.kernels.GridInterpolationKernel 
                              (gpytorch.kernels.ScaleKernel 
                                (gpytorch.kernels.RBFKernel :ard-num-dims 2))
                              :num-dims 2 :grid-size 100)
          self.feature-extractor fe))
  (defn forward [self x]
    (let [proj-x (as-> x it (self.feature-extractor it)
                            (- it (first (.min it 0)))
                            (- (* 2 (/ it (first (.max it 0)))) 1))
          mean-x (self.mean-module proj-x)
          cover-x (self.covar-module proj-x) ]
      (gpytorch.distributions.MultivariateNormal mean-x cover-x))))

(setv likelihood (gpytorch.likelihoods.GaussianLikelihood))
(setv model (GPRM train-x train-y likelihood))

(.cuda model)
(.cuda likelihood)

(setv train-iter 50)

(.train model)
(.train likelihood)

(setv optim (torch.optim.Adam [ {"params" (.parameters model.feature-extractor)} 
                                {"params" (.parameters model.covar-module)} 
                                {"params" (.parameters model.mean-module)} 
                                {"params" (.parameters model.likelihood)} ]
                              :lr 0.01))

(setv mll (gpytorch.mlls.ExactMarginalLogLikelihood likelihood model))

(defn train []
  (setv iterator (tqdm.autonotebook.tqdm (range train-iter)))
  (for [i iterator]
    (.zero-grad optim)
    (setv loss (-> train-x (model) (mll train-y) (-)))
    (.backward loss)
    (iterator.set-postfix :loss (.item loss))
    (.step optim)))

(train)

(.eval model)
(.eval likelihood)
(.cpu model)
(.cpu likelihood)

(with [_ (.no-grad torch) 
       _ (.use-toeplitz gpytorch.settings False) 
       _ (.fast-pred-var gpytorch.settings)]
  (setv preds (model valid-x)))

(print (.format "Test MAE: {}" (-> preds (. mean) 
                                         (- valid-y) 
                                         (torch.abs) 
                                         (torch.mean))))

(setv tru (-> raw 
              (get (& (= raw.L (np.random.choice (.unique raw.L)))
                      (= raw.W (np.random.choice (.unique raw.W)))))
              (.sort-values :by ["gmid"])))

(setv tru-x (-> (. (np.vstack [tru.gmid.values
                               (np.log10 tru.fug.values)]) T) 
                      (scale-x.transform) 
                      (torch.from-numpy) 
                      (.float)
                      (.contiguous)))

(with [_ (.no-grad torch) (.fast-pred-var gpytorch.settings)]
  (setv predictions (-> tru-x 
                        (model) 
                        (. mean) 
                        (.reshape -1 1) 
                        (scale-y.inverse-transform) 
                        (.flatten))))

(setv fig (plt.figure))
(plt.plot (-> tru (get "gmid") (. values)) 
          (-> tru (get "jd") (. values) (np.log10)) )
(plt.plot (-> tru (get "gmid") (. values)) 
          (np.log10 predictions))
;(plt.yscale "log")
(plt.legend ["Observation" "Predictions"])
(plt.title "Jd vs. gm/Id")
(plt.show)

;;;;;;;;; MULTI VARIATE GMID ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setv params-x ["gmid" "fug"])
(setv params-y ["jd" "L"])

;(setv raw-shuffled (.sample (get raw (& (| (= raw.W (np.random.choice (.unique raw.W)))
;                                           (= raw.W (np.random.choice (.unique raw.W)))
;                                           (= raw.W (np.random.choice (.unique raw.W))))
;                                        (> raw.L 1.6e-7))) 
;                            :frac 1))

(setv raw-shuffled (.sample (get raw (& (= raw.W (np.random.choice (.unique raw.W)))
                                        (> raw.L 1.6e-7))) 
                            :frac 1))

(setv raw-x (. (np.vstack [ raw-shuffled.gmid.values
                            (np.log10 raw-shuffled.fug.values) ]) T))
(setv raw-y (. (np.vstack [ (np.log10 raw-shuffled.jd.values) 
                            raw-shuffled.L.values ]) T))

(.fit (setx scale-x (MinMaxScaler)) raw-x)
(.fit (setx scale-y (MinMaxScaler)) raw-y)

(setv train-x (-> raw-x (scale-x.transform) 
                        (torch.from-numpy) 
                        (.float) 
                        (.contiguous)
                        (.cuda)))
(setv train-y (-> raw-y (scale-y.transform) 
                        (torch.from-numpy) 
                        (.float) 
                        (.contiguous)
                        (.cuda)))

(defclass MTGPM [gpytorch.models.ExactGP]
  (defn __init__ [self train-x train-y likelihood]
    (.__init__ (super MTGPM self) train-x train-y likelihood)
    (setv self.mean-module (gpytorch.means.MultitaskMean 
                              (gpytorch.means.ConstantMean) 
                              :num-tasks 2))
    (setv self.covar-module (gpytorch.kernels.MultitaskKernel 
                                (gpytorch.kernels.RBFKernel :ard-num-dims 2) 
                                ;(gpytorch.kernels.RQKernel 
                                ;    :ard-num-dims 2 
                                ;    :alpha-constraint (.Positive gpytorch.constraints)) 
                                :num-tasks 2 :rank 1)))
    (defn forward [self x]
      (let [mean-x (self.mean-module x)
            covar-x (self.covar-module x)]
        (gpytorch.distributions.MultitaskMultivariateNormal mean-x covar-x)))) 

(setv likelihood (gpytorch.likelihoods.MultitaskGaussianLikelihood :num-tasks 2))
(setv model (MTGPM train-x train-y likelihood))

(setv training-iterations (if (setx smoke-test (in "CI" os.environ)) 2 50))

(.cuda model)
(.cuda likelihood)
(.train model)
(.train likelihood)

(setv optimizer (torch.optim.Adam [ {"params" (.parameters model)} ] :lr 0.1))
(setv mll (gpytorch.mlls.ExactMarginalLogLikelihood likelihood model))

(setv losses
      (lfor i (range training-iterations)
        (let [_ (.zero-grad optimizer)
              output (model train-x)
              loss (- (mll output train-y))]
          (.backward loss) 
          (print (.format "Iter {}/{} - Loss: {}" 
                          (setx log-i (inc i)) 
                          training-iterations 
                          (setx log-loss (.item loss))))
          (.step optimizer)
          (, log-i log-loss))))

(.cpu model)
(.cpu likelihood)
(.eval model)
(.eval likelihood)

(setv tru (-> raw 
              (get (= raw.L (np.random.choice (.unique raw.L))))
              (get (= raw.W (np.random.choice (.unique raw.W))))
              (.sort-values :by ["gmid"])))
(setv tru-x (. (np.vstack [tru.gmid.values
                           (np.log10 tru.fug.values)]) T))
(setv valid-x (-> tru-x
                  (scale-x.transform) 
                  (torch.from-numpy) 
                  (.float)
                  (.contiguous)))

(with [_ (.no-grad torch) (.fast-pred-var gpytorch.settings)]
  (setv predictions (likelihood (model valid-x))
        prd         (-> predictions
                        (. mean)
                        (.numpy)
                        (scale-y.inverse-transform)
                        (. T)))
  (setv (, lower 
           upper )  (.confidence-region predictions)
        lw (-> lower (.numpy) (scale-y.inverse-transform) (. T))
        up (-> upper (.numpy) (scale-y.inverse-transform) (. T))))

(setv apx (pd.DataFrame {(first params-y) (** 10 (first prd))
                         (second params-y) (second prd)}))
(setv lo (pd.DataFrame {(first params-y) (** 10 (first lw))
                        (second params-y) (second lw)}))
(setv hi (pd.DataFrame {(first params-y) (** 10 (first up))
                        (second params-y) (second up)}))

(setv (, f (, y1-ax y2-ax)) (plt.subplots 1 2 :figsize (, 8 3)))
(y1-ax.plot (-> tru (get "gmid") (. values)) 
            (-> tru (get "jd") (. values)))
(y1-ax.plot (-> tru (get "gmid") (. values)) 
            (-> apx (get "jd") (. values)) )
(y1-ax.fill-between (-> tru (get "gmid") (. values))
                    (-> lo (get "jd") (. values))
                    (-> hi (get "jd") (. values))
                    :alpha 0.5)
(y1-ax.set-yscale "log")
(y1-ax.set-xlabel "gm/Id [1/V]")
(y1-ax.set-ylabel "Jd [A/m]")
(y1-ax.legend ["Observation" "Mean" "Confidence"])
(y1-ax.set-title "Jd vs. gm/Id")
(y1-ax.grid "on")
(y2-ax.plot (-> tru (get "gmid") (. values)) 
            (-> tru (get "L") (. values)) )
(y2-ax.plot (-> tru (get "gmid") (. values)) 
            (-> apx (get "L") (. values)) )
(y2-ax.fill-between (-> tru (get "gmid") (. values))
                    (-> lo (get "L") (. values))
                    (-> hi (get "L") (. values))
                    :alpha 0.5)
(y2-ax.set-xlabel "gm/Id [1/V]")
(y2-ax.set-ylabel "L [m]")
(y2-ax.legend ["Observation" "Mean" "Confidence"])
(y2-ax.set-title "L vs. gm/Id")
(y2-ax.grid "on")
(plt.show)

;;;;;;;;; MULTI VARIATE VDSAT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setv params-x ["vdsat" "fug"])
(setv params-y ["jd" "L"])

;(setv raw-shuffled (.sample (get raw (& (| (= raw.W (np.random.choice (.unique raw.W)))
;                                           (= raw.W (np.random.choice (.unique raw.W)))
;                                           (= raw.W (np.random.choice (.unique raw.W))))
;                                        (> raw.L 1.6e-7))) 
;                            :frac 1))

(setv raw-shuffled (.sample (get raw (& (= raw.W (np.random.choice (.unique raw.W)))
                                        (> raw.L 1.6e-7))) 
                            :frac 1))

(setv raw-x (. (np.vstack [ raw-shuffled.vdsat.values
                            (np.log10 raw-shuffled.fug.values) ]) T))
(setv raw-y (. (np.vstack [ (np.log10 raw-shuffled.jd.values) 
                            raw-shuffled.L.values ]) T))

(.fit (setx scale-x (MinMaxScaler)) raw-x)
(.fit (setx scale-y (MinMaxScaler)) raw-y)

(setv train-x (-> raw-x (scale-x.transform) 
                        (torch.from-numpy) 
                        (.float) 
                        (.contiguous)
                        (.cuda)))
(setv train-y (-> raw-y (scale-y.transform) 
                        (torch.from-numpy) 
                        (.float) 
                        (.contiguous)
                        (.cuda)))

(defclass MTGPM [gpytorch.models.ExactGP]
  (defn __init__ [self train-x train-y likelihood]
    (.__init__ (super MTGPM self) train-x train-y likelihood)
    (setv self.mean-module (gpytorch.means.MultitaskMean 
                              (gpytorch.means.ConstantMean) 
                              :num-tasks 2))
    (setv self.covar-module (gpytorch.kernels.MultitaskKernel 
                                (gpytorch.kernels.RBFKernel :ard-num-dims 2) 
                                ;(gpytorch.kernels.RQKernel 
                                ;    :ard-num-dims 2 
                                ;    :alpha-constraint (.Positive gpytorch.constraints)) 
                                :num-tasks 2 :rank 1)))
    (defn forward [self x]
      (let [mean-x (self.mean-module x)
            covar-x (self.covar-module x)]
        (gpytorch.distributions.MultitaskMultivariateNormal mean-x covar-x)))) 

(setv likelihood (gpytorch.likelihoods.MultitaskGaussianLikelihood :num-tasks 2))
(setv model (MTGPM train-x train-y likelihood))

(setv training-iterations (if (setx smoke-test (in "CI" os.environ)) 2 50))

(.cuda model)
(.cuda likelihood)
(.train model)
(.train likelihood)

(setv optimizer (torch.optim.Adam [ {"params" (.parameters model)} ] :lr 0.1))
(setv mll (gpytorch.mlls.ExactMarginalLogLikelihood likelihood model))

(setv losses
      (lfor i (range training-iterations)
        (let [_ (.zero-grad optimizer)
              output (model train-x)
              loss (- (mll output train-y))]
          (.backward loss) 
          (print (.format "Iter {}/{} - Loss: {}" 
                          (setx log-i (inc i)) 
                          training-iterations 
                          (setx log-loss (.item loss))))
          (.step optimizer)
          (, log-i log-loss))))

(.cpu model)
(.cpu likelihood)
(.eval model)
(.eval likelihood)

(setv tru (-> raw 
              (get (= raw.L (np.random.choice (.unique raw.L))))
              (get (= raw.W (np.random.choice (.unique raw.W))))
              (.sort-values :by ["vdsat"])))
(setv tru-x (. (np.vstack [tru.vdsat.values
                           (np.log10 tru.fug.values)]) T))
(setv valid-x (-> tru-x
                  (scale-x.transform) 
                  (torch.from-numpy) 
                  (.float)
                  (.contiguous)))

(with [_ (.no-grad torch) (.fast-pred-var gpytorch.settings)]
  (setv predictions (likelihood (model valid-x))
        prd         (-> predictions
                        (. mean)
                        (.numpy)
                        (scale-y.inverse-transform)
                        (. T)))
  (setv (, lower 
           upper )  (.confidence-region predictions)
        lw (-> lower (.numpy) (scale-y.inverse-transform) (. T))
        up (-> upper (.numpy) (scale-y.inverse-transform) (. T))))

(setv apx (pd.DataFrame {(first params-y) (** 10 (first prd))
                         (second params-y) (second prd)}))
(setv lo (pd.DataFrame {(first params-y) (** 10 (first lw))
                        (second params-y) (second lw)}))
(setv hi (pd.DataFrame {(first params-y) (** 10 (first up))
                        (second params-y) (second up)}))

(setv (, f (, y1-ax y2-ax)) (plt.subplots 1 2 :figsize (, 8 3)))
(y1-ax.plot (-> tru (get "vdsat") (. values)) 
            (-> tru (get "jd") (. values)))
(y1-ax.plot (-> tru (get "vdsat") (. values)) 
            (-> apx (get "jd") (. values)) )
(y1-ax.fill-between (-> tru (get "vdsat") (. values))
                    (-> lo (get "jd") (. values))
                    (-> hi (get "jd") (. values))
                    :alpha 0.5)
(y1-ax.set-yscale "log")
(y1-ax.set-xlabel "vdsat [V]")
(y1-ax.set-ylabel "Jd [A/m]")
(y1-ax.legend ["Observation" "Mean" "Confidence"])
(y1-ax.set-title "Jd vs. gm/Id")
(y1-ax.grid "on")
(y2-ax.plot (-> tru (get "vdsat") (. values)) 
            (-> tru (get "L") (. values)) )
(y2-ax.plot (-> tru (get "vdsat") (. values)) 
            (-> apx (get "L") (. values)) )
(y2-ax.fill-between (-> tru (get "vdsat") (. values))
                    (-> lo (get "L") (. values))
                    (-> hi (get "L") (. values))
                    :alpha 0.5)
(y2-ax.set-xlabel "vdsat [V]")
(y2-ax.set-ylabel "L [m]")
(y2-ax.legend ["Observation" "Mean" "Confidence"])
(y2-ax.set-title "L vs. vdsat")
(y2-ax.grid "on")
(plt.show)


;;;;;;;; PYRO ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
