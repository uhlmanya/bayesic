(import sys)
(import [numpy :as np])
(import [scipy :as sp])
(import [pymc3 :as pm])
(import [theano :as tn])
(import [arviz :as az])
(import [pandas :as pd])
(import [h5py :as h5])
(import [seaborn :as sb])
(import [matplotlib :as mpl])
(import [matplotlib.pyplot :as plt])
;(import [gnuplotlib :as gpl])
;(import [plotille :as plt])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])

(az.style.use "arviz-darkgrid")
(np.random.seed 666)

(setv data-path "../data/sym-op-2.hdf")
(setv min-offset 25e-3)
(setv data (as-> data-path d
                 (pd.read-hdf d "data")
                 (.dropna d)
                 (.drop-duplicates d)
                 (.reset-index d)
                 (.apply d pd.to-numeric)
                 (get d (& (< (abs (get d "totalOutput.dcOp" )) min-offset)
                           (< (get d "totalOutput.dcOp" ) 0 )))))

(for [ i (range 10) ]
  ;(setv (get data f"M{i}:jd") (/ (get data f"M{i}:id") (get data f"M{i}:W")))
  (setv (get data f"M{i}:gmid") (/ (get data f"M{i}:gm") (get data f"M{i}:id")))
  (setv (get data f"M{i}:a0") (/ (get data f"M{i}:gm") (get data f"M{i}:gds"))))

(setv (get data "abs_off") (.abs (get data "totalOutput.dcOp")))
(setv data (.rename data :columns {"totalOutput.dcOp" "dc_off"
                                   "totalOutput.sigmaOut" "sig_off"}))

(.describe (get data ["abs_off" "dc_off" "sig_off"]))
(for [c data.columns] (print c))
(list (filter #%(in "M8" %1) data.columns))

; [f"M{i}:gmid" f"M{i}:a0" f"M{i}:id"]
(setv params (flatten (lfor i [0 2 4 8] [f"M{i}:gmid" f"M{i}:fug"])))
(setv data-filtered (get data (+ ["a0"] params)))

(sb.heatmap (-> data-filtered ; data-filtered 
                (.corr) 
                ;(get ["a0"]) 
                (.transpose))
            :xticklabels True 
            :yticklabels True
            :cmap "mako")
(pd.plotting.scatter-matrix data-filtered)
(plt.show)

(setv ρ-true (-> data-filtered (.corr) (get ["a0"])))
;(setv ρ-true (-> data (.corr) (get ["offset_mean" "offset_std"])))
(print ρ-true)

(sb.heatmap (.transpose ρ-true)
            :xticklabels True 
            :yticklabels True
            :cmap "mako")
(plt.show)

(setv μ-offset (-> data (get "offset_mean") (.mean))
      σ-offset (-> data (get "offset_mean") (.std))
      μ-abs (-> data (get "offset_abs") (.mean))
      σ-abs (-> data (get "offset_abs") (.std)))

(with [model-offset (.Model pm)]
  (setv μ1 (pm.Normal "μ1" :mu μ-offset :sigma σ-offset)
        μ2 (pm.Normal "μ2" :mu μ-abs :sigma σ-abs)
        trace-offset (.sample pm)))

(with [model-offset]
  (az.plot-trace trace-offset :lines (, (, "μ1" {} [ μ-offset 
                                                     (+ μ-offset σ-offset)
                                                     (- μ-offset σ-offset) ])
                                        (, "μ2" {} [ μ-abs 
                                                     (+ μ-abs σ-abs)
                                                     (- μ-abs σ-abs) ])))
  (plt.show))



















(setv data-train (-> data-filtered 
                     (.sample 1000)))
                     ;(.set-index "a0")))

;(data-train.head)
(sb.heatmap (.corr data-train)
            :xticklabels True 
            :yticklabels True
            :cmap "mako")
(pd.plotting.scatter-matrix data-train)
(plt.show)

(with [model (pm.Model)]
  (setv ρ (pm.HalfCauchy "ρ" 5)
        η (pm.HalfCauchy "η" 5)
        σ (pm.HalfNormal "σ" 50))
  (setv M (pm.gp.mean.Linear :coeffs (.mean data-train.a0))
        K (* (** η 2) (pm.gp.cov.ExpQuad 1 ρ)))
)

















(setv coords {"tot_gain"  data-train.index 
              "gmids"     (list (filter #%(in "gmid" %1) data-train.columns))
              "a0s"       (list (filter #%(in "a0" %1) data-train.columns)) })

(with [model (pm.Model :coords coords)]
  (setv total-gain (pm.Normal "total_gain" :mu 60.0 :sd 6.0)
        dev-gmid (pm.Normal "gmid_contrib" 
                            :mu 12.0 :sd 6.0
                            :dims "gmids")
        dev-a0 (pm.Normal "a0_contrib" 
                          :mu 1500.0 :sd 1000.0
                          :dims "a0s")
        contrib (pm.Deterministic "contrib"
                                 (.sum parameter-cont)
                                 :dims "dev_param")
  )
  (setv model-data (pm.Data "data" data-train :dims (, "tot_gain" "dev_param"))
        regression (pm.Normal "likelihood" 
                              :mu parameter-corr :sd 0.5 
                              :observed model-data)))

(with [model]
  (setv trace (pm.sample 2000 :tune 2000 :target-accept 0.85 
                              :cores 12 :return-inferencedata True 
                              :random-seed 666)))





















(setv params (lfor i [0 2 4 8] [f"M{i}:gmid" f"M{i}:a0" f"M{i}:fug"]))

(setv data-filtered (get data (+ ["totalOutput.dcOp"] (flatten params))))

(sb.heatmap (.corr data-filtered)
            :xticklabels True 
            :yticklabels True
            :cmap "mako")
(plt.show)

(setv ρ-true (-> data-filtered (.corr) (get "totalOutput.dcOp")))
(print ρ-true)

(setv data-train (-> data-filtered 
                     (.sample 1000)
                     (.set-index "totalOutput.dcOp")))

(data-train.head)
(sb.heatmap (.corr data-train)
            :xticklabels True 
            :yticklabels True
            :cmap "mako")
(pd.plotting.scatter-matrix data-train)
(plt.show)





(setv param-names (+ ["a0"] (flatten (lfor i [0 2 4 8] [f"M{i}:gmid" f"M{i}:id" f"M{i}:a0"]))))

(setv param-names ["a0" "M2:gmid" "M8:a0"])
(setv data-small (-> data (get param-names) (.sample 500)))

(sb.heatmap (data-small.corr)
            :xticklabels True 
            :yticklabels True
            :cmap "mako")
(pd.plotting.scatter-matrix data-small)
(plt.show)

(setv ρ-true (-> data-small (.corr) (get "a0")))
(print ρ-true)

(sp.stats.pearsonr (-> data (get "a0") (.to-numpy))
                   (-> data (get "M2:gmid") (.to-numpy)))

(setv (, x y) (-> data-small (get ["M2:gmid" "a0"]) (. values) (. T)))

(with [model (.Model pm)]
  (setv ρ (pm.HalfCauchy "ρ" 5)
        η (pm.HalfCauchy "η" 5)
        σ (pm.HalfNormal "σ" 60))
  (setv M (pm.gp.mean.Linear :coeffs (.mean (/ y x)))
        K (* (** η 2) (pm.gp.cov.ExpQuad 1 ρ)))
  (setv a0-gp (pm.gp.Marginal :mean-func M :cov-func K))
  (a0-gp.marginal-likelihood "a0" 
                             :X (.reshape (. x T) -1 1)
                             :y (. y T) 
                             :noise σ))

(with [model]
  (setv gp-trace (pm.sample 100 :tune 20 :cores 12 :random-seed 666)))

(az.plot-trace gp-trace :var-names ["ρ" "η" "σ"])
(plt.show)

(with [model (.Model pm)]
  (setv ρ1 (pm.Normal "ρ1" :mu 0.5 :sigma 5)
        ρ2 (pm.Normal "ρ2" :mu 0.4 :sigma 5)
        ρ3 (pm.Normal "ρ3" :mu 0.5 :sigma 5)
        σ  (pm.HalfNormal "σ" :sigma 20)
        μ  (+ ρ1 (* ρ2 x) (* ρ3 (** x 2)))
        y′ (pm.Normal "y" :mu μ :sigma σ :observed y)
        trace (pm.sample 1000 :tune 2000 :cores 12 :random-seed 666)))

(az.plot-trace trace)
(plt.show)

(with [model (.Model pm)]
  (setv X1 (pm.Data "X1" (-> data-small (get "M2:gmid") (. values)))
        Y (pm.Data "Y" (-> data-small (get "a0")      (. values))))
  (setv σ (pm.HalfCauchy "σ" :beta 60 :testval 1.0)
        α (pm.Normal "α" :mu 60 :sigma 20)
        β (pm.Normal "β" :mu 0.4 :sigma 4)
        μ (+ α (* X1 β))
        y (pm.Normal "y" :mu μ :sigma σ :observed Y))
  (setv trace (pm.sample 1000 :tune 2000 :init None :cores 12)))

(az.plot-trace trace :lines (, (, "α" {} [(.mean (get data-small "a0"))])
                               (, "β" {} [(get ρ-true "M2:gmid")])))
(plt.show)


(get ρ-true "M2:gmid")













(setv (, α σ) (, 1 1)
      β [1.0 2.5]
      size 100
      X1 (np.random.randn size)
      X2 (* (np.random.randn size) 0.2)
      Y (+ α  (* (get β 0) X1) (* (get β 1) X2) (* (np.random.randn size) σ)))

(setv (, fig axs) (plt.subplots 1 2 :sharex True :figsize (, 10 4)))
(.scatter (get axs 0) X1 Y)
(.scatter (get axs 1) X2 Y)
(.set-ylabel (get axs 0) "Y")
(.set-xlabel (get axs 0) "X1")
(.set-xlabel (get axs 1) "X2")
(plt.show)

(sb.pairplot)

(setv bayesIC (.Model pm))

(with [bayesIC]
  (setv α (pm.Normal "α" :mu 0 :sigma 10)
        β (pm.Normal "β" :mu 0 :sigma 10 :shape 2)
        σ (pm.HalfNormal "σ" :sigma 1)
        μ (+ α  (* (get β 0) X1) (* (get β 1) X2))
        γ (pm.Normal "γ" :mu μ :sigma σ :observed Y)
        trace (pm.sample 100 :tune 666 :init None :cores 12)))

(setv fig (plt.figure))
(az.plot-trace trace :lines (, (, "μ1" {} [μ] )))
(plt.show)





















(setv μ (.mean (get data "totalOutput.dcOp"))
      σ (.mean (get data "totalOutput.sigmaOut")))

(.describe (get data ["totalOutput.dcOp" "totalOutput.sigmaOut"]))

(with [m (pm.Model)]
  (setv μ1 (pm.Normal "μ1" :mu μ :sigma σ)
        step (pm.NUTS)
        trace (pm.sample 2000 :tune 666 :init None :step step :cores 12)))


(setv fig (plt.figure))
(az.plot-trace trace :lines (, (, "μ1" {} [μ] )))
(plt.show)


(get data.iloc (, [666] [1 2 3]) )

(setv extremes (get data.iloc [(-> data (get "totalOutput.sigmaOut") (.argmax))
                               (-> data (get "totalOutput.sigmaOut") (.argmin))
                               (-> data (get "totalOutput.dcOp") (.argmax))
                               (-> data (get "totalOutput.dcOp") (.argmin))] ))

(.describe (get extremes ["totalOutput.sigmaOut" "totalOutput.dcOp"]))

(for [c data.columns] (print c))

(setv data-sample   (.sample data :n 1000)
      sigma-sample  (-> data-sample
                        (.get "totalOutput.sigmaOut")
                        (.to-numpy))
      dcOp-sample (-> data-sample
                        (.get "totalOutput.dcOp")
                        (.to-numpy)))


(setv fig (plt.figure))
;(sb.pairplot (get extremes ["totalOutput.sigmaOut" 
;                            "totalOutput.dcOp"
;                            "a0" "ugbw" "pm" "srr" "cmrr"
;                            #_/ ]))
;(sb.pairplot (get extremes ["Lcm1" "Wcm1" "Ldiff" "Wdiff" "Lcm2" "Wcm2" "Lcm3" "Wcm3"
;                            "Mcm11" "Mcm12" "Mcm21" "Mcm22" "Mcm31" "Mcm32" "Mdiff"
;                            #_/ ]))

(.describe (get data ["I0.M1.Au0_nmos(m).Sensitivity"
                      "I0.M4.Avt_pmos(m).Contribution"
                      "I0.M3.Avt_nmos(m).Contribution"
                      "I0.M3.Au0_nmos(m).Contribution"
                      "I0.M6.Au0_pmos(m).Sensitivity"
                      "I0.M5.Au0_pmos(m).Contribution"
                      "I0.M9.Au0_nmos(m).Sensitivity"
                      "I0.M5.Avt_pmos(m).Contribution"
                      "I0.M6.Au0_pmos(m).Contribution"
                      "I0.M1.Avt_nmos(m).Contribution"
                      "I0.M2.Au0_nmos(m).Contribution"
                      "I0.M6.Avt_pmos(m).Sensitivity"
                      "I0.M2.Au0_nmos(m).Sensitivity"
                      "I0.M0.Avt_nmos(m).Contribution"
                      "I0.M9.Avt_nmos(m).Sensitivity"
                      "I0.M1.Au0_nmos(m).Contribution"
                      "I0.M6.Avt_pmos(m).Contribution"
                      "I0.M7.Au0_pmos(m).Sensitivity"
                      "I0.M0.Au0_nmos(m).Sensitivity"
                      "I0.M7.Au0_pmos(m).Contribution"
                      "I0.M2.Avt_nmos(m).Sensitivity"
                      "I0.M1.Avt_nmos(m).Sensitivity"
                      "I0.M7.Avt_pmos(m).Sensitivity"
                      "I0.M0.Au0_nmos(m).Contribution"
                      "I0.M7.Avt_pmos(m).Contribution"
                      "I0.M8.Au0_nmos(m).Contribution"
                      "I0.M8.Avt_nmos(m).Contribution"
                      "I0.M4.Au0_pmos(m).Sensitivity"
                      "I0.M3.Avt_nmos(m).Sensitivity"
                      "I0.M9.Au0_nmos(m).Contribution"
                      "I0.M0.Avt_nmos(m).Sensitivity"
                      "I0.M4.Avt_pmos(m).Sensitivity"
                      "I0.M9.Avt_nmos(m).Contribution"
                      "totalOutput.sigmaOut"
                      "I0.M8.Au0_nmos(m).Sensitivity"
                      "totalOutput.dcOp"
                      "I0.M5.Au0_pmos(m).Sensitivity"
                      "I0.M5.Avt_pmos(m).Sensitivity"
                      "I0.M2.Avt_nmos(m).Contribution"
                      "I0.M3.Au0_nmos(m).Sensitivity"
                      "I0.M8.Avt_nmos(m).Sensitivity"
                      "I0.M4.Au0_pmos(m).Contribution"
                      #_/]))

(setv fig (plt.figure))
(sb.pairplot (get data-sample ["a0" "cmrr" 
                               "totalOutput.dcOp" 
                               "totalOutput.sigmaOut" 
                               #_/]))
(plt.show)

(setv fig (plt.figure))
(sb.pairplot (get data-sample ["a0" "ugbw" 
                               "M0:gm"
                               "M0:id"
                               "M0:fug"
                               "M1:gm"
                               "M1:id"
                               "M1:fug"
                               "M2:gm"
                               "M2:id"
                               "M2:fug"
                               "M3:gm"
                               "M3:id"
                               "M3:fug"
                               "M4:gm"
                               "M4:id"
                               "M4:fug"
                               "M5:gm"
                               "M5:id"
                               "M5:fug"
                               "M5:gm"
                               "M6:id"
                               "M0:fug"
                               "M0:gm"
                               "M0:id"
                               "M0:fug"
                               #_/]))
(plt.show)





(.describe (get data ["totalOutput.sigmaOut" "totalOutput.dcOp"]))

(setv fig (plt.figure))
(sb.pairplot (get data-sample ["totalOutput.sigmaOut" 
                               "totalOutput.dcOp"
                               #_/ ]))
(plt.show)

(.describe (get data-sample ["totalOutput.sigmaOut" "totalOutput.dcOp"]))
(.describe (-> data-sample (.sample :n 1000) (get ["totalOutput.sigmaOut" "totalOutput.dcOp"])))
(.describe (-> data-sample (.sample :n 100) (get ["totalOutput.sigmaOut" "totalOutput.dcOp"])))

(setv (, fig axs) (plt.subplots 1 3 :sharey False :tight-layout True))
(.hist (get axs 0) (np.random.choice sigma-sample 100) :bins 50)
(.hist (get axs 0) (np.random.choice dcOp-sample 100) :bins 50)
(.hist (get axs 1) (np.random.choice sigma-sample 1000) :bins 50)
(.hist (get axs 1) (np.random.choice dcOp-sample 1000) :bins 50)
(.hist (get axs 2) sigma-sample :bins 50)
(.hist (get axs 2) dcOp-sample :bins 50)
(plt.show)

(for [c data.columns] (print c))


(setv params-X ["CL" "I0" "Lcm1" "Wcm1" "Ldiff" "Wdiff" "Lcm2"
                "Wcm2" "Lcm3" "Wcm3" "Mcm11" "Mcm12" "Mcm21"
                "Mcm22" "Mcm31" "Mcm32" "Mdiff"]
      dims-X (len params-X)
      obs-X (-> data-sample 
              (.sample :n 1000) 
              (get params-X)
              (. values)))

(setv params-Y ["totalOutput.dcOp" "totalOutput.sigmaOut"]
      (, μ-Y σ-Y) (. (-> data-sample 
                         (.sample :n 1000) 
                         (get params-Y))
                     values T))

(setv num-induction-points 333
      ind-X (-> data-sample 
                (.sample :n num-induction-points)
                (get params-X)
                (. values)))

(with [model (.Model pm)]
  (setv l (pm.HalfCauchy "l" :beta 3 :shape (, dims-X))
        σ-k (pm.HalfCauchy "sf2" :beta 3)
        σ-n (pm.Normal "sn2" :mu (.mean σ-Y) :sigma (.std σ-Y))
        K (* (pm.gp.cov.ExpQuad dims-X l) (** σ-k 2))
        gp-spatial (pm.gp.MarginalSparse :cov-func K :approx "FITC")
        obs (gp-spatial.marginal-likelihood "obs" :X obs-X :Xu ind-X :y μ-Y :noise σ-n)
        ;mp (pm.find-MAP)
        trace (pm.sample 42 :tune 13 :cores 12)
        ))

(setv fig (plt.figure))
(pm.traceplot trace)
;(pm.plot-posterior trace)
(plt.show)






(setv mu (np.zeros 3))
(setv cv (np.array [[1.0 0.5 0.1]
                    [0.5 2.0 0.2]
                    [0.1 0.2 1.0]]))

(with [mv-model (pm.Model)]
  (setv dt (np.random.multivariate-normal mu cv 10)
        sd-dist (pm.Exponential.dist 1.0 :shape 3)
        (, chol corr stds) (pm.LKJCholeskyCov "chol-cov" 
                                              :n 3 :eta 2 
                                              :sd-dist sd-dist 
                                              :compute-corr True)
        vals (pm.MvNormal "vals" :mu mu :chol chol :observed dt)
        trace (pm.sample 42 :tune 13 :cores 12)
        ))

(setv fig (plt.figure))
(pm.traceplot trace)
(plt.show)













(setv num-prediction-points 666
      truth-sample (.sample data :n num-prediction-points)
      prd-X (-> truth-sample (get params-X) (. values))
      tru-Y (-> truth-sample (get params-Y) (. values)))

(with [model]
  (setv offset-prediction (gp-spatial.conditional "ofs" prd-X)
        samples (pm.sample-posterior-predictive [mp] 
                                                :var-names ["ofs"] 
                                                :samples 100)))

(setv (, tru-μ tru-σ) (-> truth-sample (get params-Y) (. values T)))

(setv prd-μ (-> samples (get "ofs") (.mean 0))
      prd-σ (-> samples (get "ofs") (.std 0)))

(setv fig (plt.figure))
(plt.scatter (range 0 num-prediction-points) tru-μ :alpha 0.3)
(plt.scatter (range 0 num-prediction-points) prd-μ :alpha 0.3)
(plt.show)

(setv fig (plt.figure))
(plt.show)









(for [c data.columns] (print c))

(setv data-sample (data.sample :n 1000)
      train-sample (data-sample.sample :n 100)
      train-x (-> train-sample (get ["CL" "I0"
                                     "Lcm1" "Wcm1" "Mcm11" "Mcm12"
                                     "Ldiff" "Wdiff" "Mdiff"
                                     "Lcm2" "Wcm2" "Mcm21" "Mcm22"
                                     "Lcm3" "Wcm3" "Mcm31" "Mcm32"
                                     #_/ ])
                              (.to-numpy))
      train-y (-> train-sample (get ["a0" "ugbw" "cmrr"]) 
                              (.to-numpy)) )

(setv data-x (tn.shared train-x)
      data-y (tn.shared train-y))

(setv fig-pair (plt.figure))
(sb.pairplot (get data-sample [
                               "Ldiff" "Wdiff"
                               "a0" "ugbw" 
                               #_/ ]))
(plt.show)











(np.random.seed 666)
(setv n 20)
(setv X (get (np.sort (* 3 (np.random.rand n))) (, (slice None) None)))

(with [model (.Model pm)]
  (setv l-true 0.3
        s2-f-true 1.0
        cov (* s2-f-true (pm.gp.cov.ExpQuad 1 l-true))
        s2-n-true 0.1
        K-noise (* (** s2-n-true 2) (tn.tensor.eye n))
        K (+ (cov X) K-noise)))

(setv K ((tn.function [] (+ (cov X) K-noise))))
(setv y (np.random.multivariate-normal (np.zeros n) K))

(setv fig (plt.figure :figsize (, 14 5))
      ax (fig.add-subplot 111))
(ax.plot X y "ok" :ms 10)
(ax.set-xlabel "x")
(ax.set-ylabel "f(x)")
(plt.show)

(setv Z (get (np.linspace 0 3 100) (, (slice None) None)))

(with [model]
  (setv l (pm.Uniform "l" 0 10)
        s2-f-log (pm.Uniform "s2-f-log" :lower -10 :upper 5)
        s2-f (pm.Deterministic "s2-f" (tn.tensor.exp s2-f-log))
        s2-n-log (pm.Uniform "s2-n-log" :lower -10 :upper 5)
        s2-n (pm.Deterministic "s2-n" (tn.tensor.exp s2-n-log))
        f-cov (* s2-f (pm.gp.cov.ExpQuad 1 l))
        ;y-obs (pm.gp.TP "y_obs" :cov-func f-cov :sigma s2-n :observed {"X" X "Y" y})))
        gp (pm.gp.Marginal :cov-func f-cov)
        y-obs (gp.marginal-likelihood "y-obs" :noise s2-n :X X :y y)))

(with [model]
  (setv trace (pm.sample 42 :tune 666 :cores 12)))

(setv fig (plt.figure))
(pm.traceplot trace)
;(pm.plot-posterior trace)
(plt.show)



(setv a 1.0
      b 0.1
      c 1.5
      d 0.75)
      
(setv X0 [10.0 5.0])

(setv size 25)
(setv time 15)
(setv τ (np.linspace 0 time size))

(defn dX/dt [X t a b c d]
  (np.array [(- (* a (get X 0)) (* b (get X 0) (get X 1))) 
             (+ (* (- c) (get X 1)) (* d b (get X 0) (get X 1)))]))

(defn competition-model [a b c d]
  (sp.integrate.odeint dX/dt :y0 X0 :t τ :rtol 0.01 :args (, a b c d)))

(defn add-noise [a b c d]
  (as-> size it
    (, it 2)
    (np.random.normal :size it)
    (+ (competition-model a b) it)))

;(setv observed (add-noise a b c d))
(setv observed (competition-model a b c d))

(setv (, _ ax) (plt.subplots :figsize (, 12 4)))
(ax.plot (get observed (, (slice None) 0)) "x" :label "prey")
(ax.plot (get observed (, (slice None) 1)) "x" :label "pred")
(ax.set-xlabel "time")
(ax.set-ylabel "population")
(ax.set-title "Observed Data")
(ax.legend)
(plt.show)

(with [model-lv (.Model pm)]
  (setv a (pm.HalfNormal "a" 1.0)
        b (pm.HalfNormal "b" 1.0)
        c (pm.HalfNormal "c" 1.0)
        d (pm.HalfNormal "d" 1.0)
        sim (pm.Simulator "sim" competition-model 
                          :params (, a b c d) 
                          :epsilon 7 
                          :observed observed)
        trace-lv (pm.sample-smc :kernel "ABC" :parallel True)
        idata-lv (az.from-pymc3 trace-lv)))

(plt.figure)
(az.plot-trace idata-lv :kind "rank_vlines")
(plt.show)

(plt.figure)
(az.plot-posterior idata-lv)
(plt.show)

(setv (, _ ax) (plt.subplots :figsize (, 14 6)))
(ax.plot (get observed (, (slice None) 0)) "x" :label "prey")
(ax.plot (get observed (, (slice None) 1)) "x" :label "pred")
(ax.plot (competition-model (-> trace-lv (get "a")  (.mean))
                            (-> trace-lv (get "b") (.mean))
                            (-> trace-lv (get "c") (.mean))
                            (-> trace-lv (get "d") (.mean)))
         :linewidth 3)
(for [i (np.random.randint 0 size 75)]
  (setv sim (competition-model (-> trace-lv (get "a") (get i)) 
                               (-> trace-lv (get "b") (get i))
                               (-> trace-lv (get "c") (get i))
                               (-> trace-lv (get "d") (get i))))
  (ax.plot (get sim (, (slice None) 0)) :alpha 0.1 :c "C0")
  (ax.plot (get sim (, (slice None) 1)) :alpha 0.1 :c "C1"))
(ax.set-xlabel "time")
(ax.set-xlabel "population")
(ax.legend)
(plt.show)









(setv data-path "../data/ptmn90.hdf")

(setv data (with [f (h5.File data-path "r")]
  (let [column-names (list (map (fn [c] (c.decode "UTF-8")) (get f "columns")))
        data-matrix (-> f (get "data") (np.array) (np.transpose))
        data-frame (pd.DataFrame data-matrix :columns column-names)]
    (-> data-frame (.dropna) (.reset-index) (.apply pd.to-numeric)))))

(setv (get data ["Vgs" "Vds" "Vbs"]) (-> data (get ["Vgs" "Vds" "Vbs"]) (.round 2))
      (get data "gmid") (/ (get data "gm") (get data "id"))
      (get data "Jd") (/ (get data "id") (get data "W")))

(setv data-sample (get data (& (= data.Vbs 0.0)
                               (= data.Vds 0.6)
                               (= data.W (first (data.W.sample :n 1)))
                               (= data.L (first (data.L.sample :n 1))))))

;(setv data-values (. (get data ["W" "L" "gm" "id" "fug"]) values))
;(setv data-dct (sp.fftpack.dctn data-values :norm "ortho"))

(setv obs-x (-> data-sample (get ["W" "L"]) (. values))
      obs-y (-> data-sample (get ["id"]) (. values)))

(setv num-xu 15)
(setv (, w l) (np.meshgrid (np.linspace (min data.W) (max data.W) num-xu) 
                           (np.linspace (min data.L) (max data.L) num-xu)))
(setv induction-x (np.concatenate [(w.reshape (* num-xu num-xu) 1) 
                                   (l.reshape (* num-xu num-xu) 1)] 1))

(setv fig (plt.figure))
(plt.scatter (get data-values (, (slice None) 4)))
(plt.scatter )
(plt.show)

(with [model (.Model pm)]
  (setv l (pm.HalfCauchy "l" :beta 3 :shape (, 2))
        σ-f (pm.HalfCauchy "σ-f" :beta 3)
        σ-n (pm.HalfCauchy "σ-n" :beta 3)
        K (* (pm.gp.cov.ExpQuad 2 l) (** σ-f 2))
        gp (pm.gp.MarginalSparse :cov-func K :approx "FITC")
        obs (gp.marginal-likelihood "obs" :Xu induction-x :X obs-x :y obs-y :noise σ-n)
        trace (pm.sample 42 :tune 666 :cores 12)))



(setv sub-sample (data-sample.sample :n 25))
(setv (, x y) (. (get sub-sample ["Vgs" "id"]) values T))
(setv y (-> y (np.log10) #_(np.abs)))

(setv fig-tf (plt.figure))
(setv ax (plt.scatter x  y))
;(setv ax (data-sample.plot.scatter :x "Vgs" :y "id" :c "k" :s 1.2))
(plt.show)

(with [simple-model (pm.Model)]
  (let [β (pm.Normal "β" :mu (- 7.0) :sigma 2.0 :shape 4) 
        σ (pm.HalfNormal "σ" :sigma 0.5)
        μ (+ (get β 0) (* (get β 1) x)
             (* (get β 2) (** x 2) )
             (* (get β 3) (** x 3)))
        i-d (pm.Normal "i-d" :mu μ :sigma σ :observed y) ]
    (setv simple-trace (pm.sample 1000 :tune 2000 :cores 10))))

(pm.traceplot simple-trace)
(pm.plot-posterior simple-trace)
(plt.show)

(setv x-pred (np.linspace 0 1.2 100))

(setv fig-tf (plt.figure))
(setv ax (plt.scatter x  y))
(as.set-ylim 0 None)
(for [(, β0 β1 β2 β3) (get (get simple-trace "β") (slice None 20)) ]
  (plt.plot x-pred (+ β0 (* β1 x-pred) 
                         (* β2 (** x-pred 2))
                         (* β3 (** x-pred 3)))
            :alpha 0.3 ))
(plt.show)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setv data-path "../data/sym-amp.h5")

(setv data (-> data-path
               (pd.read-hdf "data")
               (.dropna)
               (.drop-duplicates)
               (.reset-index)
               (.apply pd.to-numeric)))

(setv sigma-sample (-> data 
                       (.sample :n 10000)
                       (get "totalOutput.sigmaOut")
                       (.to-numpy)))

(setv dcOp-sample (-> data 
                      (.sample :n 10000)
                      (get "totalOutput.dcOp")
                      (.to-numpy)))

;(setv (, fig axs) (plt.subplots 1 3 :sharey False :tight-layout True))
;(.hist (get axs 0) (np.random.choice sigma-sample 100) :bins 50)
;(.hist (get axs 0) (np.random.choice dcOp-sample 100) :bins 50)
;(.hist (get axs 1) (np.random.choice sigma-sample 1000) :bins 50)
;(.hist (get axs 1) (np.random.choice dcOp-sample 1000) :bins 50)
;(.hist (get axs 2) sigma-sample :bins 50)
;(.hist (get axs 2) dcOp-sample :bins 50)
;(plt.show)

(for [c data.columns] (print c))

(setv data-sample (data.sample :n 1000)
      train-sample (data-sample.sample :n 100)
      train-x (-> train-sample (get ["CL" "I0"
                                     "Lcm1" "Wcm1" "Mcm11" "Mcm12"
                                     "Ldiff" "Wdiff" "Mdiff"
                                     "Lcm2" "Wcm2" "Mcm21" "Mcm22"
                                     "Lcm3" "Wcm3" "Mcm31" "Mcm32"
                                     #_/ ])
                              (.to-numpy))
      train-y (-> train-sample (get ["a0" "ugbw" "cmrr"]) 
                              (.to-numpy)) )

(setv data-x (tn.shared train-x)
      data-y (tn.shared train-y))

(setv fig-pair (plt.figure))
(sb.pairplot (get data-sample [
                               "Ldiff" "Wdiff"
                               "a0" "ugbw" 
                               #_/ ]))
(plt.show)



(setv data-path "../data/ptmn90.hdf")

(setv data (-> data-path
               (pd.read-hdf "data")
               (.dropna)
               (.drop-duplicates)
               (.reset-index)
               (.apply pd.to-numeric)))

(setv basic-model (pm.Model))

(with basic-model
  (setv α-a0
        β-a0
        α-ugbw
        β-ugbw
        μ-cmrr
        σ-cmrr

        

        μ (+ α (train-x.dot β) )
        a0   (get (data-y.get-value) (, (slice None) 0))
        ugbw (get (data-y.get-value) (, (slice None) 1))
        cmrr (get (data-y.get-value) (, (slice None) 2))
        obs-a0 (pm.Weibull )
  ))

(pm.traceplot trace)
(pm.plot-posterior trace)
(plt.show)


(with basic-model
  (display (az.summary trace)))
